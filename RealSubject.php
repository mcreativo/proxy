<?php


class RealSubject implements Subject
{

    public function __construct()
    {
        usleep(10000);
    }

    public function doSomething()
    {
        echo 'doing something' . PHP_EOL;
    }
}
