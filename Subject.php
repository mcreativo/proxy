<?php


interface Subject
{
    public function doSomething();
} 