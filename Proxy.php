<?php


class Proxy implements Subject
{

    /**
     * @var RealSubject
     */
    protected $realSubject = null;

    public function doSomething()
    {
        if ($this->realSubject == null) {
            $this->realSubject = new RealSubject();
        }
        $this->realSubject->doSomething();
    }


}